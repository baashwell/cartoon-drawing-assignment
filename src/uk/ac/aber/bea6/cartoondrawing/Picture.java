import java.awt.*;
import java.awt.Image;
import java.awt.image.ImageObserver;

import javax.swing.*;



public class Picture implements ImageObserver {

protected int x;
protected int y;
protected java.awt.Image image;
protected String filename;

public Picture(String filename, int x, int y)
{
	this.filename = filename;
	this.x = x;
	this.y = y;
}


public void drawPicture(Graphics g)
{
	g.drawImage(image,x,y,this);
}

public void convertToImage()
{
	image = Toolkit.getDefaultToolkit().getImage(filename);
}

public Integer getX()
{
	return x;
}

public Integer getY()
{
	return y;
}

@Override
public boolean imageUpdate(Image arg0, int arg1, int arg2, int arg3, int arg4,
		int arg5) {
	// TODO Auto-generated method stub
	return false;
}
}
