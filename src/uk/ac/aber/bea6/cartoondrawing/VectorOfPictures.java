import java.awt.*;
import java.util.*;
public class VectorOfPictures {
	
	private Vector<Picture> pictures;
	Picture p;
	
	public VectorOfPictures() {
		pictures = new Vector<Picture>();
	}
	public void addPicture(Picture p)
	{
		pictures.add(p);
	}
	
	public void clearPictures()
	{
		pictures.removeAllElements();
	}
	
	public void drawAll(Graphics g)
	{
		Picture pic;
		for(int i=0; i < pictures.size(); i++) {
			pic = (Picture)(pictures.get(i));
			if (pic instanceof Image){
				Image img = (Image)pic;
				img.convertToImage();
				pic.drawPicture(g);
			}
			else {
				TextBubble text = (TextBubble)pic;
				text.convertToImage();
				text.drawText(g);
			}
	
	}
	}
	public void removepicture(int x, int y)
	{
		Picture pic;
		
		for(int i = 0; i < pictures.size(); i++) 
		{
			pic = (Picture)(pictures.get(i));
			int picx = pic.getX();
			int picy = pic.getY();
			
			if ((picx - 60 < x && x < picx + 60) && (picy - 60 < y && y < picy + 60) )
			{
				pictures.remove(i);
			}
		}	
	}
}
