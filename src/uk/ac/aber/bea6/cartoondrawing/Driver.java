
/**
 * The Driver class contains the main method that starts the entire programme off.
 * @author Ben Ashwell, Martin Key, Abel Toro
 */
public class Driver 
{
	/**
	 * Main Method creates the frame
	 * @param args
	 */
	public static void main(String[] args)
	{
		CartoonFrame cf = new CartoonFrame();
	}

}
