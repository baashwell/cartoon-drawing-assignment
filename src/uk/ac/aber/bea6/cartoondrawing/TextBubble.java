import java.awt.Graphics;



public class TextBubble extends Picture {

	private String caption;
	private String file;
	
	public TextBubble(String FileName, String caption, int x, int y) {
		super(FileName, x,y);
		this.caption = caption;
		file = FileName;
		}
	
	public void setCaption(String s) {
		s = caption;
		}

	public String getCaption() {
		return caption;
		}
	
	public void drawText(Graphics g)
	{
		g.drawImage(image, x,y,this);
		x += 30;
		y += 80;
		g.drawString(caption,x,y);
	}

}
