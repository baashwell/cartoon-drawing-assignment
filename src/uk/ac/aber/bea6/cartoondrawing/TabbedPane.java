import java.awt.*;
import javax.swing.*;

public class TabbedPane extends JTabbedPane{

	private String file;
	
	public TabbedPane() {
		
		int x = 1;
				
		JPanel panel1 = new ButtonPanel(new GridLayout(1,4), x, this);
		x ++;
		JPanel panel2 = new ButtonPanel(new GridLayout (1,4), x, this);
		x ++;
		JPanel panel3 = new ButtonPanel(new GridLayout (1,4), x, this);
		x ++;
		JPanel panel4 = new ButtonPanel(new GridLayout (1,4), x, this);

		this.addTab("Boys", null, panel1);
		this.addTab("Girls", null, panel2);
		this.addTab("Shapes", null, panel3);
		this.addTab("Text Box", null, panel4);
		
	}
	
	public void setFile(String s)
	{
		file = s; 
	}
	public String getFile()
	{
		return file;
	}
}


	