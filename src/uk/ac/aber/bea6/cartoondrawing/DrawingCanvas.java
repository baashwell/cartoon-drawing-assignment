import javax.swing.*;

import java.awt.*;
import java.awt.image.ImageObserver;

/**
 * DrawingCanvas class extends Jpanel and is used to set up the panel which will be used to place the images on.
 */
public class DrawingCanvas extends JPanel
{
	private VectorOfPictures vop;
	/**
	 * This constructor sets the size and colour of the drawing canvas
	 */
	DrawingCanvas()
	{
		vop = new VectorOfPictures();
		this.setBackground(Color.white);
		this.setPreferredSize(new Dimension(700, 400));
	}
	/**
	 * Paint Component method calls the super method and then runs method
	 * draw all in the VectorOfPictures Object
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		vop.drawAll(g);
	}
	/**
	 * This Method creates a Picture Object using the parameters given
	 * it then runs the add picture method in the VectorOfPictures Object
	 * and then lastly repaints the canvas
	 * @param filename
	 * @param x
	 * @param y
	 */
	public void addPic(String filename, int x, int y)
	{
			//change coords so appears in middle of click
			//but means all pictures must be the same size 170px * 170px
			x -= 87;
			y -= 87;
			Image i = new Image(filename,x,y);
			vop.addPicture(i);
			repaint();
	}
	
	/** Method to add a text box to the canvas */
	public void addText(String filename, String caption, int x, int y)
	{
		x -= 87;
		y -= 87;
		TextBubble text = new TextBubble(filename,caption,x,y);
		vop.addPicture(text);
		repaint();
	}
	/**
	 * This method clears the screen by calling the VectorOfPictures
	 * method clearPictures and then repaints the canvas
	 */
	public void clear()
	{
		vop.clearPictures();
		repaint();
	}
	/**
	 * This method calls removepicture to remove one image from the canvas
	 * then repaints the canvas
	 */
	public void removepicture(int x, int y)
	{
		vop.removepicture(x, y);
		repaint();
	}

}
