import java.awt.*;

import javax.swing.*;

/**
 * ButtonPanel class extends JPanel and is used to create the different buttons
 * found in the tabs at the top of the programme.
 * @author Ben Ashwell, Martin Key, Abel Toro 
 */
public class ButtonPanel extends JPanel {
	
	/**
	 * 4 different Buttons for each of the tabs.
	 */
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	/**
	 * Button listner to listen for clicks on the buttons.
	 */
	private ButtonListener bl;
	/**
	 * Link to the TabbedPane.
	 */
	private TabbedPane tabPane;
	

	/**
	 * Constructor for the button panel.
	 */
	public ButtonPanel(GridLayout gridLayout, int x, TabbedPane tp) {
		
		bl = new ButtonListener(this);
		tabPane = tp;
		if(x==1)
			panel1();
		if(x==2)
			panel2();
		if(x==3)
			panel3();
		if(x==4)
			panel4();

		this.add(button1);
		this.add(button2);
		this.add(button3);
		this.add(button4);
		
		
	}
	
	/**
	 * Formatting the buttons for the first tab on the tabbed pane.
	 */
	public void panel1(){
		
		Icon icon1 = new ImageIcon("boyhappy.png");
		Icon icon2 = new ImageIcon("boysad.png");
		Icon icon3 = new ImageIcon("boytongue.png");
		Icon icon4 = new ImageIcon("boyangry.png");
		
		button1 = new JButton(icon1);
		button1.setActionCommand("1");
		button1.addActionListener(bl);
		
		button2 = new JButton(icon2);
		button2.setActionCommand("2");
		button2.addActionListener(bl);
		
		button3 = new JButton(icon3);
		button3.setActionCommand("3");
		button3.addActionListener(bl);
		
		button4 = new JButton(icon4);
		button4.setActionCommand("4");
		button4.addActionListener(bl);
	}
	/**
	 * Formatting the buttons for the second tabbed pane.
	 */
	public void panel2(){
		Icon icon1 = new ImageIcon("girlhappy.png");
		Icon icon2 = new ImageIcon("girlsad.png");
		Icon icon3 = new ImageIcon("girltongue.png");
		Icon icon4 = new ImageIcon("girlangry.png");
		
		button1 = new JButton(icon1);
		button1.setActionCommand("5");
		button1.addActionListener(bl);
		
		button2 = new JButton(icon2);
		button2.setActionCommand("6");
		button2.addActionListener(bl); 
		
		button3 = new JButton(icon3);
		button3.setActionCommand("7");
		button3.addActionListener(bl);
		
		button4 = new JButton(icon4);
		button4.setActionCommand("8");
		button4.addActionListener(bl);
	}
	/**
	 * Formatting the buttons for the third tabbed pane.
	 */
	public void panel3(){
		Icon icon1 = new ImageIcon("circle.png");
		Icon icon2 = new ImageIcon("square.png");
		Icon icon3 = new ImageIcon("triangle.png");
		Icon icon4 = new ImageIcon("star.png");
		
		button1 = new JButton(icon1);
		button1.setActionCommand("9");
		button1.addActionListener(bl);
		
		button2 = new JButton(icon2);
		button2.setActionCommand("10");
		button2.addActionListener(bl); 
		
		button3 = new JButton(icon3);
		button3.setActionCommand("11");
		button3.addActionListener(bl);
		
		button4 = new JButton(icon4);
		button4.setActionCommand("12");
		button4.addActionListener(bl);
	}
	
	public void panel4(){
		Icon icon1 = new ImageIcon("roundtextleft.png");
		Icon icon2 = new ImageIcon("roundtextright.png");
		Icon icon3 = new ImageIcon("bubbleleft.png");
		Icon icon4 = new ImageIcon("bubbleright.png");
		
		button1 = new JButton(icon1);
		button1.setActionCommand("13");
		button1.addActionListener(bl);
		
		button2 = new JButton(icon2);
		button2.setActionCommand("14");
		button2.addActionListener(bl); 
		
		button3 = new JButton(icon3);
		button3.setActionCommand("15");
		button3.addActionListener(bl);
		
		button4 = new JButton(icon4);
		button4.setActionCommand("16");
		button4.addActionListener(bl);
	}
	/**
	 * Returning the file name of the chosen image for other parts of the programme.
	 */
	public void setFile(String s)
	{
		tabPane.setFile(s);
	}
	
}
